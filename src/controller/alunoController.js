module.exports = class alunoController {


    //Postar Docente
    static async postDocente(req, res) {
        const {nome, idade, profissao, cursoMatriculado} = req.body;

        // console.log(nome);
        // console.log(idade);
        // console.log(profissao);
        // console.log(cursoMatriculado);
        console.log(req.body);

        res.status(200).json({message: "Cadastrado"})
    }

    //Atualizar Aluno
    static async updateAluno(req, res) {
        const {nome, idade, profissao, cursoMatriculado} = req.body;

        if (nome != '' && idade != 0 && profissao != '' && cursoMatriculado != ''){
            res.status(200).json({ message: "Aluno Editado" });
        }
        else {
            res.status(400).json({ message: "Dados incompletos" });
        }
    }

    //Deletar Aluno
    static async deletarAluno(req, res) {
        if (req.params.id != null){
            res.status(200).json({ message: "Aluno Excluido" });
        }
        else{
            res.status(400).json({ message: "Aluno não encontrado" });
        }
    }

}
