const connect = require("../dbExercicio/connect(E)");

module.exports = class dbControllerExercicio{

    static async getTablesName(req, res){
        //consulta para obter a lista de tabelas
        const queryShowTables = "show tables";

        connect.query(queryShowTables, async function(err, result, fields){
            if(err){
                console.log(err);
                return res.status(500).json({error: "Erro ao obter tabelas do banco de dados",err});
            }//fim do if

            //extrai os nomes das tabelas de forma organizada
            const tableNames = result.map(row => row[fields[0].name]);

            res.status(200).json({message: "Os nomes das tabelas que compõe o banco de dado 'Venda de Ingresso'", tableNames});
            console.log("Tabelas do banco de dados:", tableNames);

        })//fechamento connect query
    }

    static async getTablesAtribute(req, res){
        //consulta para obter a lista de tabelas
        const queryShowTables = "show tables";

        connect.query(queryShowTables, async function(err, result, fields){
            //Organização e descrição das tabelas do banco
            const tables = [];

            //Iterar sobre os resultados para obter a descrição de cada tabela
            for(let i = 0; i < result.length; i++){
                const tableName = result[i][`Tables_in_${connect.config.connectionConfig.database}`]

                //acionando comando desc
                const queryDescTable = `describe ${tableName}`;

                try{
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function(err, result, fields){
                            if(err){
                                reject(err);
                            }
                            resolve(result);
                        })
                    })
                    tables.push({name: tableName, description: tableDescription});
                } 
                catch (error){
                    console.log(error)
                    return res.status(500).json({error: "Erro ao obter a descrição da tabela!"});
                }//fim do catch
            }// fim do for

            res.status(200).json({message: "As informações dos atributos das tabelas do banco de dados 'Venda de Ingressos'", tables});

        })//fechamento connect query
    }
}
