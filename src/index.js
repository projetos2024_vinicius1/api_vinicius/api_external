const express = require('express')
const app = express()
const testConnect = require('./db/testConnect')
const testConnectExercicio = require('./dbExercicio/testConnect(E).js')

class AppController {
    constructor() {
        this.express = express();
        this.middlewares();
        this.routes();
        this.testConnect = testConnect();
        // this.testConnectExercicio = testConnectExercicio();
    }

    middlewares() {
        this.express.use(express.json());
    }

    routes() {
        const apiRoutes = require('./routes/apiRoutes')
        this.express.use('/teste/', apiRoutes);
    }
}

module.exports = new AppController().express;
