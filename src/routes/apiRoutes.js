//Constantes
const router = require('express').Router();
const teacherController = require('../controller/teacherController');
const alunoController = require('../controller/alunoController');
const JSONPlaceholderController = require('../controller/JSONPlaceholderController');
const dbController = require('../controller/dbController');
const dbControllerExercicio = require('../controller/dbExercicioController');
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController");

//Rotas básicas de post, get, put e delete
router.get('/ALGUMA-COISA/', teacherController.getAlgumaCoisa),
router.post('/cadastroAluno/', alunoController.postDocente),
router.put('/listarAluno/', alunoController.updateAluno),
router.delete('/deletarAluno/:id', alunoController.deletarAluno),

//Rotas JSONPlaceholderController
router.get("/external", JSONPlaceholderController.getUsers),
router.get("/UsersWebsiteIO", JSONPlaceholderController.getUsersWebsiteIO),
router.get("/UsersWebsiteCOM", JSONPlaceholderController.getUsersWebsiteCOM),
router.get("/UsersWebsiteNET", JSONPlaceholderController.getUsersWebsiteNET),
router.get('/external/filter', JSONPlaceholderController.getCountDomain),

//rota para consulta das tabelas do banco
router.get("/tables", dbController.getTables),

//Exercicio do dia 11/04/2024
router.get("/nomeTabelas", dbControllerExercicio.getTablesName),
router.get("/nomeAtributos", dbControllerExercicio.getTablesAtribute),

//rota para cadastro e select de cliente
router.post("/postCliente/", clienteController.createCliente),
router.get("/getClientes/", clienteController.getAllClientes),
router.get("/getClientes2/", clienteController.getAllClientes2),

//rotas tabelas pizzaria
router.get("/listarPedidosPizza/", pizzariaController.listarPedidosPizza),
router.get("/listarPedidosPizzasComJoin/", pizzariaController.listarPedidosPizzasComJoin),

module.exports = router
